class Sprite {
    constructor(x, y, width, height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
    getX() {
        return this.x;
    }
    getY() {
        return this.y;
    }
    setX(x) {
        this.x = x;
    }
    setY(y) {
        this.y = y;
    }
    getWidth() {
        return this.width;
    }
    getHeight() {
        return this.height;
    }
    draw(canvas) {
        var context = canvas.getContext('2d');
        context.fillRect(this.x, this.y, this.width, this.height);
    }
    contains(x, y) {
        return this.x <= x && x <= this.x + this.width && this.y <= y && y <= this.y + this.height;
    }
}
class Renderer {
    constructor() {
        this.drawables = [];
    }
    draw(canvas) {
        for (var i = 0; i < this.drawables.length; i++) {
            this.drawables[i].draw(canvas);
        }
    }
    addDrawable(drawable) {
        this.drawables.push(drawable);
    }
}
class Camera extends Sprite {
    constructor(world, x, y, width, height) {
        super(x, y, width, height);
        this.world = world;
    }
    scale(scale, clientX, clientY) {
        var oldWidth = this.width, oldHeight = this.height;
        this.width *= scale;
        this.height *= scale;
        this.x += (oldWidth - this.width) * (clientX / this.world.width);
        this.y += (oldHeight - this.height) * (clientY / this.world.height);
    }
    scroll(deltaX, deltaY) {
        var newX = this.x + deltaX, newY = this.y + deltaY;
        if (newX > 0 && newX < this.world.width - this.width) {
            this.x = newX;
        }
        if (newY > 0 && newY < this.world.height - this.height) {
            this.y = newY;
        }
    }
    draw(canvas) {
        var context = canvas.getContext('2d');
        context.drawImage(this.world, this.x, this.y, this.width, this.height, 0, 0, canvas.width, canvas.height);
    }
}
class Card extends Sprite {
    constructor(x, y, width, height, inHand, image) {
        super(x, y, width, height);
        this.inHand = inHand;
        this.number = 1;
        this.color = 2;
        this.shape = 3;
        this.image = image;
        this.highlightColor = "black";
    }
    getInHand() {
        return this.inHand;
    }
    setInHand(inHand) {
        this.inHand = inHand;
    }
    getNumber() {
        return this.number;
    }
    getColor() {
        return this.color;
    }
    getShape() {
        return this.shape;
    }
    draw(canvas) {
        var context = canvas.getContext('2d');
        /*
        context.shadowBlur = 10;
        if (this.inHand) {
            context.shadowColor = this.highlightColor;
        }
        */
        if (this.inHand) {
            context.shadowBlur = 10;
            context.shadowColor = this.highlightColor;
        }
        else {
            context.shadowBlur = 1;
            context.shadowColor = "black";
        }
        context.drawImage(this.image, this.x, this.y, this.width, this.height);
        context.shadowBlur = 0;
    }
    setHighlightColor(color) {
        this.highlightColor = color;
    }
}
class Tile extends Sprite {
    constructor(x, y, width, height) {
        super(x, y, width, height);
        this.validNumbers = [1];
        this.validColors = [2];
        this.validShapes = [4];
    }
    draw(canvas) {
        if (this.content) {
            this.content.draw(canvas);
        }
        else {
            var context = canvas.getContext('2d');
        }
    }
    setContent(card) {
        card.setX(this.x);
        card.setY(this.y);
        this.content = card;
    }
    canContain(card) {
        if (this.validNumbers.indexOf(card.getNumber()) > -1 && this.validColors.indexOf(card.getColor()) > -1
            && this.validShapes.indexOf(card.getShape()) > -1) {
            return true;
        }
        else {
            return false;
        }
    }
    setValid(numbers, colors, shapes) {
        this.validColors = colors;
        this.validNumbers = numbers;
        this.validShapes = shapes;
    }
}
class Table extends Renderer {
    constructor(rows, columns, tileWidth, tileHeight) {
        super();
        for (var i = 0; i < rows; i++) {
            for (var j = 0; j < columns; j++) {
                this.drawables.push(new Tile(i * tileWidth, j * tileHeight, tileWidth, tileHeight));
            }
        }
    }
    draw(canvas) {
        var context = canvas.getContext("2d");
        var gradient = context.createRadialGradient(canvas.width / 2, canvas.height / 2, canvas.height / 10, 0, canvas.height / 2, canvas.width);
        gradient.addColorStop(0, "white");
        gradient.addColorStop(1, "gray");
        context.fillStyle = gradient;
        context.fillRect(0, 0, canvas.width, canvas.height);
        for (var i = 0; i < this.drawables.length; i++) {
            this.drawables[i].draw(canvas);
        }
    }
    placeCard(card, camera, canvas) {
        var tile = this.getTileForCard(card, camera, canvas);
        if (tile != null) {
            tile.setContent(card);
            card.setInHand(false);
        }
    }
    checkPlace(card, camera, canvas) {
        var tile = this.getTileForCard(card, camera, canvas);
        if (tile != null) {
            card.setHighlightColor("green");
        }
        else {
            card.setHighlightColor("red");
        }
    }
    getTileForCard(card, camera, canvas) {
        var x = camera.getX() + card.getX() * camera.getWidth() / canvas.width + card.getWidth() / 2, y = camera.getY() + card.getY() * camera.getHeight() / canvas.height + card.getHeight() / 2;
        for (var i = 0; i < this.drawables.length; i++) {
            if (this.drawables[i].contains(x, y) && this.drawables[i].canContain(card)) {
                return this.drawables[i];
            }
        }
        return null;
    }
    getTile(index) {
        return this.drawables[index];
    }
}
class OffScreenRenderer extends Renderer {
    constructor(canvas, width, height) {
        super();
        canvas.width = width;
        canvas.height = height;
        this.canvas = canvas;
    }
    render() {
        super.draw(this.canvas);
    }
}
class ScreenRenderer extends Renderer {
    constructor(canvas, camera, table, offScreenRenderer, width, height) {
        super();
        canvas.width = width;
        canvas.height = height;
        this.table = table;
        this.offScreenRenderer = offScreenRenderer;
        this.canvas = canvas;
        this.drawables.push(camera);
        this.camera = camera;
        this.hand = new Hand(canvas);
        this.addDrawable(this.hand);
        canvas.onmousedown = (event) => {
            this.selectedCard = this.hand.getCard(event.clientX, event.clientY);
            if (this.selectedCard != null) {
                this.initX = this.selectedCard.getX();
                this.initY = this.selectedCard.getY();
                this.isDrag = true;
                return;
            }
            this.isScroll = true;
            this.prevX = event.clientX;
            this.prevY = event.clientY;
        };
        canvas.onmouseup = (event) => {
            this.isScroll = false;
            if (this.isDrag) {
                this.isDrag = false;
                this.table.placeCard(this.selectedCard, this.camera, this.canvas);
                if (this.selectedCard.getInHand()) {
                    this.selectedCard.setX(this.initX);
                    this.selectedCard.setY(this.initY);
                    this.selectedCard.setHighlightColor("black");
                }
                this.offScreenRenderer.render();
                this.clear();
                this.render();
            }
        };
        canvas.onmousemove = (event) => {
            if (this.isDrag) {
                this.selectedCard.setX(event.clientX - this.selectedCard.getWidth() / 2);
                this.selectedCard.setY(event.clientY - this.selectedCard.getHeight() / 2);
                this.table.checkPlace(this.selectedCard, this.camera, this.canvas);
                this.clear();
                this.render();
            }
            else if (this.isScroll) {
                this.camera.scroll(this.prevX - event.clientX, this.prevY - event.clientY);
                this.prevX = event.clientX;
                this.prevY = event.clientY;
                this.clear();
                this.render();
            }
        };
    }
    render() {
        super.draw(this.canvas);
    }
    clear() {
        var context = this.canvas.getContext('2d');
        context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
}
class Hand extends Renderer {
    constructor(canvas) {
        super();
        var image = new Image();
        image.src = '/images/rc1.png';
        this.drawables.push(new Card(canvas.width / 2 - 230, canvas.height - 150, 100, 100, true, image));
        image = new Image();
        image.src = '/images/gt2.png';
        this.drawables.push(new Card(canvas.width / 2 - 110, canvas.height - 150, 100, 100, true, image));
        image = new Image();
        image.src = '/images/yx3.png';
        this.drawables.push(new Card(canvas.width / 2 + 130, canvas.height - 150, 100, 100, true, image));
        image = new Image();
        image.src = '/images/br4.png';
        this.drawables.push(new Card(canvas.width / 2 + 10, canvas.height - 150, 100, 100, true, image));
    }
    getCard(x, y) {
        for (var i = 0; i < this.drawables.length; i++) {
            if (this.drawables[i].contains(x, y) && this.drawables[i].getInHand()) {
                return this.drawables[i];
            }
        }
        return null;
    }
    draw(canvas) {
        for (var i = 0; i < this.drawables.length; i++) {
            if (this.drawables[i].getInHand()) {
                this.drawables[i].draw(canvas);
            }
        }
    }
}
window.onload = () => {
    var TABLE_SIZE = 3400;
    var offScreenCanvas = document.createElement('canvas'), offScreenRenderer = new OffScreenRenderer(offScreenCanvas, TABLE_SIZE, TABLE_SIZE);
    var table = new Table(34, 34, 100, 100), initTile = table.getTile(16 * 34 + 16);
    var image = new Image();
    image.src = '/images/rx4.png';
    initTile.setContent(new Card(0, 0, 100, 100, false, image));
    image.onload = () => {
        offScreenRenderer.render();
        screenRenderer.render();
    };
    var allValid = [1, 2, 3, 4];
    table.getTile(16 * 34 + 15).setValid(allValid, allValid, allValid);
    table.getTile(16 * 34 + 17).setValid(allValid, allValid, allValid);
    table.getTile(15 * 34 + 16).setValid(allValid, allValid, allValid);
    table.getTile(17 * 34 + 16).setValid(allValid, allValid, allValid);
    offScreenRenderer.addDrawable(table);
    offScreenRenderer.render();
    var screenRenderer = new ScreenRenderer(document.getElementById('canvas'), new Camera(offScreenCanvas, TABLE_SIZE / 2 - window.innerWidth / 2, TABLE_SIZE / 2 - window.innerHeight / 2, window.innerWidth, window.innerHeight), table, offScreenRenderer, window.innerWidth, window.innerHeight);
    screenRenderer.render();
};
//# sourceMappingURL=app.js.map